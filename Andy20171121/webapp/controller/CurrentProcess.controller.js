sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History"
], function(Controller,JSONModel,History) {
	"use strict";

	return Controller.extend("Andy20171121.controller.CurrentProcess", {

		onInit: function () {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("sign").attachPatternMatched(this._onObjectMatched, this);
		},
		_onObjectMatched: function (oEvent) {
			
			var oArgs, oView;

			oArgs = oEvent.getParameter("arguments");
		//	console.log(oArgs);
			oView = this.getView();
			//var RequisitionId = oArgs.RequisitionId.toString();
			var RequisitionId = "456779fd-e5d0-4a39-bfa2-0a58f6c7ad76";			
		     var oJsonModel = new JSONModel("/flow7/api/dashboard/next/" + RequisitionId);

             oView.setModel(oJsonModel,"ProcessingDetail");
            //this.getView().byId("detailPage").setTitle(oEvent.getParameter("arguments")["?query"].diagramName);
            
		},

		getRouter : function () {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},

		onNavBack: function (oEvent) {
			var oHistory, sPreviousHash;

			oHistory = History.getInstance();
			sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				this.getRouter().navTo("list", {}, true /*no history*/);
			}
		}
		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf Andy20171121.view.CurrentProcess
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf Andy20171121.view.CurrentProcess
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf Andy20171121.view.CurrentProcess
		 */
		//	onExit: function() {
		//
		//	}

	});

});