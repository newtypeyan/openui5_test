sap.ui.define([
	"sap/ui/core/mvc/Controller",	
	"sap/ui/core/routing/History",
	"sap/ui/model/json/JSONModel",
	"Andy20171121/model/formatter"
], function(Controller,History,JSONModel,formatter) {
	"use strict";

	return Controller.extend("Andy20171121.controller.Detail", {
		formatter: formatter,
		onInit: function () {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("detail").attachPatternMatched(this._onObjectMatched, this);
		},
		_onObjectMatched: function (oEvent) {
			
			var oArgs, oView;

			oArgs = oEvent.getParameter("arguments");
		//	console.log(oArgs);
			oView = this.getView();
			/*
		     var oJsonModel = new JSONModel("/flow7/api/fdp/m/" + oArgs.RequisitionId.toString());

             oView.setModel(oJsonModel,"DiagramDetail");
            // var oData=this.getView().getModel("DiagramDetail");
            // console.log(oData);
            */
            var RequisitionId = oArgs.RequisitionId.toString();
            var that = this;
            this.getDetailModel(RequisitionId).then(function(){
        		    var oDialog = that.getView().byId("BusyDialog");
        		    var sIntervalCallId=jQuery.sap.intervalCall(500,that,function(){
        		    if (that.getView().byId("StatusButton"))
        		    {
        		    	//console.log(jQuery(".sapMListTblNavCol").size());
        		    	jQuery.sap.clearIntervalCall(sIntervalCallId);
		 				oDialog.close();
        		    }
        		    });
            });
            //var oJsonModelStep = new JSONModel("/flow7/api/dashboard/process/" + oArgs.RequisitionId.toString());
            //oView.setModel(oJsonModelStep,"ProcessingDetail");
            //console.log(oEvent.getParameter("arguments")["?query"].diagramName);
            this.getView().byId("detailPage").setTitle(oEvent.getParameter("arguments")["?query"].diagramName);
            
		},
		
        getDetailModel : function (RequisitionId) {
        	    var that = this;
        	    return new Promise(function (resolve, reject) {
        	    	var oDialog = that.getView().byId("BusyDialog");
		 			oDialog.open();
        	        var oJsonModelStep = new JSONModel("/flow7/api/dashboard/process/" + RequisitionId);
        	        that.getView().setModel(oJsonModelStep,"ProcessingDetail");
			        if ( that.getView("ProcessingDetail") ) {
			            resolve(oJsonModelStep);
			        } else {
			            reject("get json error");
			        }
			    });
        },		
		
		getRouter : function () {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},

		onNavBack: function (oEvent) {
			var oHistory, sPreviousHash;

			oHistory = History.getInstance();
			sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				this.getRouter().navTo("list", {}, true /*no history*/);
			}
		},

		onPress: function (oEvent) {
			var oItem, oCtx,oBindItem;

			oItem = oEvent.getSource();
			//console.log(oItem);
			oCtx = oItem.getBindingContext("ProcessingDetail");
			//oCtx = oItem.getBindingContext("undefined");
			oBindItem = oCtx.getModel().getProperty(oCtx.getPath());
			var RequisitionId = oBindItem.RequisitionId;
            if (RequisitionId !== undefined && RequisitionId !== "")
            {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("sign",{
					RequisitionId : RequisitionId,
					query:{
						//diagramName: oBindItem.DiagramName,
						//diagramId: oBindItem.DiagramId
					}
				});	
            }
		}
		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf Andy20171121.view.Detail
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf Andy20171121.view.Detail
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf Andy20171121.view.Detail
		 */
		//	onExit: function() {
		//
		//	}

	});

});