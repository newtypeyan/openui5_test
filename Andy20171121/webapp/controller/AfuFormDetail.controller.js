sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel"
], function(Controller,JSONModel) {
	"use strict";

	return Controller.extend("Andy20171121.controller.AfuFormDetail", {

		onInit: function () {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("detail").attachPatternMatched(this._onObjectMatched, this);
		},
		_onObjectMatched: function (oEvent) {
			
			var oArgs, oView;

			oArgs = oEvent.getParameter("arguments");
		//	console.log(oArgs);
			oView = this.getView();
		     var oJsonModel = new JSONModel("/flow7/api/fdp/m/" + oArgs.RequisitionId.toString());

             oView.setModel(oJsonModel,"DiagramDetail");
            // var oData=this.getView().getModel("DiagramDetail");
            // console.log(oData);
/*            
            var oJsonModelStep = new JSONModel("/flow7/api/dashboard/process/" + oArgs.RequisitionId.toString());
            oView.setModel(oJsonModelStep,"ProcessingDetail");
            //console.log(oEvent.getParameter("arguments")["?query"].diagramName);
            this.getView().byId("detailPage").setTitle(oEvent.getParameter("arguments")["?query"].diagramName);
*/            
		}

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf Andy20171121.view.AfuFormDetail
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf Andy20171121.view.AfuFormDetail
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf Andy20171121.view.AfuFormDetail
		 */
		//	onExit: function() {
		//
		//	}

	});

});