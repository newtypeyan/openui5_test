sap.ui.define([
	"sap/ui/core/mvc/Controller",
		"sap/ui/model/json/JSONModel"
], function(Controller,JSONModel) {
	"use strict";

	return Controller.extend("Andy20171121.controller.Master", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf Andy20171121.view.Master
		 */
			onInit: function() {
				     var oJsonModel = new JSONModel("/flow7/api/dashboard/processing/folder");
        			this.getView().setModel(oJsonModel,"DiagramList");
        			
			},
			
			onListItemPressed: function (oEvent) {
			var oItem, oCtx,oBindItem;
			oItem = oEvent.getSource();
			//console.log(oItem);
			oCtx = oItem.getBindingContext("DiagramList");
			oBindItem = oCtx.getModel().getProperty(oCtx.getPath());

				var oRouter =sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("list",{
					DiagramId : oCtx.getProperty("Key/DiagramId"),
					query:{
						diagramName: oBindItem.Key.DiagramName,
						diagramId: oBindItem.Key.DiagramId
					}
				});	
            
		}
		

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf Andy20171121.view.Master
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf Andy20171121.view.Master
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf Andy20171121.view.Master
		 */
		//	onExit: function() {
		//
		//	}

	});

});