sap.ui.define([
	"sap/ui/core/Control",
	"sap/m/Label",
	"sap/m/Input",
	"sap/m/TableSelectDialog",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/model/json/JSONModel"
], function (Control, Label, Input,TableSelectDialog, Filter, FilterOperator,JSONModel) {
	"use strict";
	var GetSupplier = Control.extend("newtype.sap.generic.andy.GetSupplier", {
		__valueHelpCurrentFilter: [],
		metadata : {
			properties : {
				value: 	{type : "string", defaultValue : ""},
				selectDialogFilterKeys : {type : "string[]", group : "Appearance", defaultValue : null}
			},
			aggregations : {
				_label : {type : "sap.m.Label", multiple: false, visibility : "hidden"},
				_input : {type : "sap.m.Input", multiple: false, visibility : "hidden"},
				__selectDialog : {type : "sap.m.TableSelectDialog", multiple: false, visibility : "hidden"}
			},
			events : {
				change : {
					parameters : {
						value : {type : "string"}
					}
				}
			}
			
		},
		init : function () {
			this.setAggregation("_label", new Label({
				text: "{i18n>PaySupplierName}"
			}).addStyleClass("sapUiTinyMargin"));
			this.setAggregation("_input", new Input({
				type : "Text",
				textFormatMode : "Value",
				valueHelpOnly : true,
				showValueHelp : true,
				valueHelpRequest : this.__onValueHelpRequest.bind(this)				
			}));
			
			var oInput = this.getAggregation("_input");
			Input.prototype.init.call(oInput);
			//var that = oInput;
			this.__valueHelpCurrentFilter = [];
			oInput.attachValueHelpRequest(this.__onValueHelpRequest, this);	
			
			this.setAggregation("__selectDialog", new TableSelectDialog({
				// PROPERTIES
				title: "{i18n>SupplierTitle}",
				noDataText: "{i18n>noData}",
				multiSelect: false,
				// EVENTS
				search: this.__onValueHelpSearch.bind(this),
				confirm: this.__onValueHelpConfirm.bind(this),
				columns: [
						new sap.m.Column({
							hAlign: "Begin",
							header: new sap.m.Label({
								text: "{i18n>Ma002}"
							})
						}),
						new sap.m.Column({
							hAlign: "Begin",
							header: new sap.m.Label({
								text: "{i18n>Ma003}"
							})
						}),
						new sap.m.Column({
							hAlign: "Begin",
							header: new sap.m.Label({
								text: "{i18n>Ma005}"
							})
						}),
						new sap.m.Column({
							hAlign: "Begin",
							header: new sap.m.Label({
								text: "{i18n>Ma021}"
							})
						})						
				]
				
			}));
			
			var oJsonModelPurma = new JSONModel("/erp/api/purma");
    		//this.getView().setModel(oJsonModel2,"Purma"); 
			var oTemplate = new sap.m.ColumnListItem({
				cells: [
						new sap.m.Text({
						text: "{Ma002}"
						}),
						new sap.m.Text({
						text: "{Ma003}"
						}),
						new sap.m.Text({
						text: "{Ma005}"
						}),
						new sap.m.Text({
						text: "{Ma021}"
						})						
				]
			});

		this.getAggregation("__selectDialog").setModel(oJsonModelPurma);
		this.getAggregation("__selectDialog").bindItems("/", oTemplate);			
		
		this.getAggregation("__selectDialog").setMultiSelect(false);
		},
		

		__onValueHelpConfirm: function(oEvent) {
			/*
			var selectedItems = oEvent.getParameter("selectedItems");
			var oInput = this.getAggregation("_input");
			//var that = oInput;	
			var sourceItemTemplate = this.getBindingInfo("selectDialogItems").template;
			//console.log(selectedItems);
			//console.log(sourceItemTemplate.getBindingInfo("title"));
					for( var ik in selectedItems ) {
						var selectedItem = selectedItems[ik];
						var keyValue = this.__getSourceValueFromValueHelp(selectedItem, sourceItemTemplate.getBindingInfo("title"));
						this.getAggregation("_input").setValue(keyValue);
					}
			*/		
			var aContexts = oEvent.getParameter("selectedContexts");
			if (aContexts)
			{			
	            var sSupplierName = aContexts.map(function(oContext) { 
	            	var oBindItem = oContext.getModel().getProperty(oContext.getPath());
	            	return oBindItem.Ma003; 
	            }).join(", ");
	            var sSupplierId = aContexts.map(function(oContext) { 
	            	var oBindItem = oContext.getModel().getProperty(oContext.getPath());
	            	return oBindItem.Ma005; 
	            }).join(", "); 
	            var sCurrency = aContexts.map(function(oContext) { 
	            	var oBindItem = oContext.getModel().getProperty(oContext.getPath());
	            	return oBindItem.Ma021; 
	            }).join(", "); 
	            
	            this.getAggregation("_input").setValue(sSupplierName);
	            this.setValue(sSupplierName);
	            var oFormModel = this.getModel("Diagram");
	            //console.log(oFormModel);
	            if (oFormModel)
	            {
	            	oFormModel.setProperty("/PaySupplierId",sSupplierId);
	            	oFormModel.setProperty("/Currency",sCurrency);
	            }
	            oEvent.getSource().getBinding("items").filter([]);
	            
			}
		},		

		__onValueHelpRequest: function(oEvent) {
				var selectDialog = this.getAggregation("__selectDialog");
				selectDialog.open();
		},
	
		__onValueHelpSearch: function(oEvent) {
			/*
			var aInternalFilters = [];
			var sSearchValue = oEvent.getParameter("value");

			aInternalFilters = this.addSearchFilter(sSearchValue);

			var aFilters = aInternalFilters.length > 0 ? new Filter([new Filter(aInternalFilters, false)].concat(this.__valueHelpCurrentFilter), true) : this.__valueHelpCurrentFilter;

			oEvent.getSource().getBinding("items").filter( aFilters );
			*/
			var sValue = oEvent.getParameter("value");
			var oFilter = new Filter(
				"Ma003",
				sap.ui.model.FilterOperator.Contains, sValue
			);
			oEvent.getSource().getBinding("items").filter([oFilter]);			
		},

		//////////////////////////////////////////////
		// METHODS
		//////////////////////////////////////////////
	
		addSearchFilter: function(sSearchValue) {
			var aInternalFilters = [];
			var aFilterKeys = this.getSelectDialogFilterKeys();
			
			if( sSearchValue !== undefined && sSearchValue !== null && sSearchValue.length > 0 ) {
				for( var k in aFilterKeys ) {
					aInternalFilters.push(new Filter(
						aFilterKeys[k],
						FilterOperator.Contains,
						sSearchValue
					));
				}
			}
			
			return aInternalFilters;
		},
		
		

		setValue: function (iValue) {
			this.setProperty("value", iValue, true);
			this.getAggregation("_input").setValue(iValue);
		},

		renderer : function (oRM, oControl) {
			oRM.write("<div");
			oRM.writeControlData(oControl);
			oRM.addClass("myCustomControl");
			oRM.writeClasses();
			oRM.write(">");
			//oRM.write("<div class=\"sapUiNoMargin\">");
			oRM.renderControl(oControl.getAggregation("_label"));
			//oRM.write("</div>");
			oRM.write("<div>");
			oRM.renderControl(oControl.getAggregation("_input"));
			oRM.write("</div>");
			oRM.write("</div>");
		}
	});

	/*
	 * Forwards a function call to a managed object based on the aggregation name.
	 * If the name is items, it will be forwarded to the list, otherwise called locally
	 * @private
	 * @param {string} sFunctionName The name of the function to be called
	 * @param {string} sAggregationName The name of the aggregation asociated
	 * @returns {any} The return type of the called function
	 */
	GetSupplier.prototype._callMethodInManagedObject = function (sFunctionName, sAggregationName) {
		var aArgs = Array.prototype.slice.call(arguments);

		if (sAggregationName === "selectDialogItems") {
			// apply to the internal list
			var subArgs = aArgs.slice(1);
			subArgs[0] = "items";
			return this.getAggregation("__selectDialog")[sFunctionName].apply(this.getAggregation("__selectDialog"), subArgs);
		} else {
			// apply to this control
			return sap.ui.base.ManagedObject.prototype[sFunctionName].apply(this, aArgs.slice(1));
		}
	};

	/**
	 * Forwards aggregations with the name of items to the internal list.
	 * @override
	 * @protected
	 * @param {string} sAggregationName The name for the binding
	 * @param {object} oBindingInfo The configuration parameters for the binding
	 * @returns {sap.m.MultiInputDialog} this pointer for chaining
	 */
	GetSupplier.prototype.bindAggregation = function () {
		var args = Array.prototype.slice.call(arguments);

		// propagate the bind aggregation function to list
		this._callMethodInManagedObject.apply(this, ["bindAggregation"].concat(args));
		return this;
	};

	GetSupplier.prototype.validateAggregation = function (sAggregationName, oObject, bMultiple) {
		return this._callMethodInManagedObject("validateAggregation", sAggregationName, oObject, bMultiple);
	};

	GetSupplier.prototype.setAggregation = function (sAggregationName, oObject, bSuppressInvalidate) {
		this._callMethodInManagedObject("setAggregation", sAggregationName, oObject, bSuppressInvalidate);
		return this;
	};

	GetSupplier.prototype.getAggregation = function (sAggregationName, oDefaultForCreation) {
		return this._callMethodInManagedObject("getAggregation", sAggregationName, oDefaultForCreation);
	};

	GetSupplier.prototype.indexOfAggregation = function (sAggregationName, oObject) {
		return this._callMethodInManagedObject("indexOfAggregation", sAggregationName, oObject);
	};

	GetSupplier.prototype.insertAggregation = function (sAggregationName, oObject, iIndex, bSuppressInvalidate) {
		this._callMethodInManagedObject("insertAggregation", sAggregationName, oObject, iIndex, bSuppressInvalidate);
		return this;
	};

	GetSupplier.prototype.addAggregation = function (sAggregationName, oObject, bSuppressInvalidate) {
		this._callMethodInManagedObject("addAggregation", sAggregationName, oObject, bSuppressInvalidate);
		return this;
	};

	GetSupplier.prototype.removeAggregation = function (sAggregationName, oObject, bSuppressInvalidate) {
		return this._callMethodInManagedObject("removeAggregation", sAggregationName, oObject, bSuppressInvalidate);
	};

	GetSupplier.prototype.removeAllAggregation = function (sAggregationName, bSuppressInvalidate) {
		return this._callMethodInManagedObject("removeAllAggregation", sAggregationName, bSuppressInvalidate);
	};

	GetSupplier.prototype.destroyAggregation = function (sAggregationName, bSuppressInvalidate) {
		this._callMethodInManagedObject("destroyAggregation", sAggregationName, bSuppressInvalidate);
		return this;
	};

	GetSupplier.prototype.getBinding = function (sAggregationName) {
		return this._callMethodInManagedObject("getBinding", sAggregationName);
	};


	GetSupplier.prototype.getBindingInfo = function (sAggregationName) {
		return this._callMethodInManagedObject("getBindingInfo", sAggregationName);
	};

	GetSupplier.prototype.getBindingPath = function (sAggregationName) {
		return this._callMethodInManagedObject("getBindingPath", sAggregationName);
	};
	
	/*
	* Override the exit method to free local resources and destroy 
	* @public
	*/	
	GetSupplier.prototype.exit = function() {
		// do something here to free resources ;)
		this.__valueHelpCurrentFilter = undefined;
    };	
    
	return GetSupplier;
	
});