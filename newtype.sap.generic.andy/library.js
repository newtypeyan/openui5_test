/* global newtype:true */

sap.ui.define([
		"jquery.sap.global",
		"sap/ui/core/library"
	], // library dependency
	function(jQuery) {

		"use strict";

		sap.ui.getCore().initLibrary({
			name: "newtype.sap.generic.andy",
			version: "1.0.0",
			dependencies: ["sap.ui.core"],
			types: [],
			interfaces: [],
			controls: [
				"newtype.sap.generic.andy.GetSupplier"
			],
			elements: []
		});

		return newtype.sap.generic.andy;

	}, /* bExport= */ false);