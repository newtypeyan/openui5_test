sap.ui.define([
	"sap/ui/core/Control",
	"sap/m/Label",
	"sap/m/Input",
	"sap/m/TableSelectDialog",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/model/json/JSONModel"
], function (Control, Label, Input,TableSelectDialog, Filter, FilterOperator,JSONModel) {
	"use strict";
	var GetHomeDept = Control.extend("newtype.sap.generic.andy.GetDept", {
		__valueHelpCurrentFilter: [],
		metadata : {
			properties : {
				value: 	{type : "string", defaultValue : ""},
				selectDialogFilterKeys : {type : "string[]", group : "Appearance", defaultValue : null}
			},
			aggregations : {
				_label : {type : "sap.m.Label", multiple: false, visibility : "hidden"},
				_input : {type : "sap.m.Input", multiple: false, visibility : "hidden"},
				__selectDialog : {type : "sap.m.TableSelectDialog", multiple: false, visibility : "hidden"}
			},
			events : {
				change : {
					parameters : {
						value : {type : "string"}
					}
				}
			}
			
		},
		init : function () {
			this.setAggregation("_label", new Label({
				text: "{i18n>HomeDept}"
			}).addStyleClass("sapUiTinyMargin"));
			this.setAggregation("_input", new Input({
				type : "Text",
				textFormatMode : "Value",
				valueHelpOnly : true,
				showValueHelp : true,
				valueHelpRequest : this.__onValueHelpRequest.bind(this)				
			}));
			
			var oInput = this.getAggregation("_input");
			Input.prototype.init.call(oInput);
			//var that = oInput;
			this.__valueHelpCurrentFilter = [];
			oInput.attachValueHelpRequest(this.__onValueHelpRequest, this);	
			
			this.setAggregation("__selectDialog", new TableSelectDialog({
				// PROPERTIES
				title: "{i18n>DeptTitle}",
				noDataText: "{i18n>noData}",
				multiSelect: false,
				// EVENTS
				search: this.__onValueHelpSearch.bind(this),
				confirm: this.__onValueHelpConfirm.bind(this),
				columns: [
						new sap.m.Column({
							hAlign: "Begin",
							header: new sap.m.Label({
								text: "{i18n>Me001}"
							})
						}),
						new sap.m.Column({
							hAlign: "Begin",
							header: new sap.m.Label({
								text: "{i18n>Me002}"
							})
						})
				]
				
			}));
			
			var oJsonModelCmsme = new JSONModel("/erp/api/cmsme");
    		
			var oTemplate = new sap.m.ColumnListItem({
				cells: [
						new sap.m.Text({
						text: "{Me001}"
						}),
						new sap.m.Text({
						text: "{Me002}"
						})
				]
			});

		this.getAggregation("__selectDialog").setModel(oJsonModelCmsme);
		this.getAggregation("__selectDialog").bindItems("/", oTemplate);			
		
		this.getAggregation("__selectDialog").setMultiSelect(false);
		},
		

		__onValueHelpConfirm: function(oEvent) {
		
			var aContexts = oEvent.getParameter("selectedContexts");
			if (aContexts)
			{			
	            var sDeptName = aContexts.map(function(oContext) { 
	            	var oBindItem = oContext.getModel().getProperty(oContext.getPath());
	            	return oBindItem.Me002; 
	            }).join(", ");
	            var sDeptId = aContexts.map(function(oContext) { 
	            	var oBindItem = oContext.getModel().getProperty(oContext.getPath());
	            	return oBindItem.Me001; 
	            }).join(", "); 
	            
	            this.getAggregation("_input").setValue(sDeptName);
	            this.setValue(sDeptName);
	            /*
	            var oFormModel = this.getModel("Diagram");
	            //console.log(oFormModel);
	            if (oFormModel)
	            {
	            	oFormModel.setProperty("/HomeDept",sDeptName);
	            	oFormModel.setProperty("/HomeDept",sDeptName);
	            }
	            */
	            oEvent.getSource().getBinding("items").filter([]);
			}
		},		

		__onValueHelpRequest: function(oEvent) {
				var selectDialog = this.getAggregation("__selectDialog");
				selectDialog.open();
		},
	
		__onValueHelpSearch: function(oEvent) {
			/*
			var aInternalFilters = [];
			var sSearchValue = oEvent.getParameter("value");

			aInternalFilters = this.addSearchFilter(sSearchValue);

			var aFilters = aInternalFilters.length > 0 ? new Filter([new Filter(aInternalFilters, false)].concat(this.__valueHelpCurrentFilter), true) : this.__valueHelpCurrentFilter;

			oEvent.getSource().getBinding("items").filter( aFilters );
			*/
			var sValue = oEvent.getParameter("value");
			var oFilter = new Filter(
				"Me002",
				sap.ui.model.FilterOperator.Contains, sValue
			);
			oEvent.getSource().getBinding("items").filter([oFilter]);			
		},

		//////////////////////////////////////////////
		// METHODS
		//////////////////////////////////////////////
	
		addSearchFilter: function(sSearchValue) {
			var aInternalFilters = [];
			var aFilterKeys = this.getSelectDialogFilterKeys();
			
			if( sSearchValue !== undefined && sSearchValue !== null && sSearchValue.length > 0 ) {
				for( var k in aFilterKeys ) {
					aInternalFilters.push(new Filter(
						aFilterKeys[k],
						FilterOperator.Contains,
						sSearchValue
					));
				}
			}
			
			return aInternalFilters;
		},
		
		

		setValue: function (iValue) {
			this.setProperty("value", iValue, true);
			this.getAggregation("_input").setValue(iValue);
		},

		renderer : function (oRM, oControl) {
			oRM.write("<div");
			oRM.writeControlData(oControl);
			oRM.addClass("myCustomControl");
			oRM.writeClasses();
			oRM.write(">");
			oRM.renderControl(oControl.getAggregation("_label"));
			oRM.write("<div>");
			oRM.renderControl(oControl.getAggregation("_input"));
			oRM.write("</div>");
			oRM.write("</div>");
		}
	});

	return GetHomeDept;
	
});