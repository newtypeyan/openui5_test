sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"sap/ui/model/resource/ResourceModel"
], function(Controller,MessageToast,ResourceModel) {
	"use strict";

	return Controller.extend("andy20171107.controller.View1", {
		onInit : function () {
			 // set i18n model on view
		     var i18nModel = new ResourceModel({
		        bundleName: "andy20171107.i18n.i18n"
		     });
         this.getView().setModel(i18nModel, "i18n");
		},
	    onShowHello : function () {
			var oBundle = this.getView().getModel("i18n").getResourceBundle();
			var sMsg = oBundle.getText("title");
	         MessageToast.show(sMsg);
	    }
	});
});