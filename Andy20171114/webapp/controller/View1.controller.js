sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel"
], function(Controller,JSONModel) {
	"use strict";

	return Controller.extend("Andy20171114.controller.View1", {
		onInit : function () {
			 //var oJsonModel = new JSONModel("/erp/api/purma");
		     var oJsonModel = new JSONModel("/flow7/api/dashboard/processing");

         this.getView().setModel(oJsonModel);

         //var oData=this.getView().getModel();
         //console.log(oData);
		},
		
		onListItemPressed: function (oEvent) {
			var oItem, oCtx;
			oItem = oEvent.getSource();
			oCtx = oItem.getBindingContext();
            if (oCtx.getProperty("DiagramId")==="FDP_P0")
            {
				var oRouter =sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("detail",{
					RequisitionId : oCtx.getProperty("RequisitionId")
				});	
            }
			
		}
		
	});
});