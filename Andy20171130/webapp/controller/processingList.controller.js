sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageToast",
	"Andy20171130/model/formatter"	
], function(Controller,JSONModel,MessageToast,formatter) {
	"use strict";

	return Controller.extend("Andy20171130.controller.processingList", {

		formatter: formatter,
		onInit : function () {
		     //var oJsonModel = new JSONModel("/flow7/api/dashboard/processing");
        	//this.getView().setModel(oJsonModel,"processingLists");

        	//var oData=this.getView().getModel("processingLists");
        	//console.log(oData);
        	var that = this;
        	this.getListModel().then(function(){
        		    var oDialog = that.getView().byId("BusyDialog");
        		    var sIntervalCallId = jQuery.sap.intervalCall(500,that,function(){
        		    if ( jQuery(".sapMListTblNavCol").size() > 1 )
        		    {
        		    	//console.log(jQuery(".sapMListTblNavCol").size());
        		    	jQuery.sap.clearIntervalCall(sIntervalCallId);
		 				oDialog.close();
        		    }
        		    });
        	});
        	this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			//this._oRouter.getRoute("master").attachMatched(this._handleRouteMatched, this);
		},
        getListModel : function (){
        	    var that = this;
        	    return new Promise(function (resolve, reject) {
        	    	var oDialog = that.getView().byId("BusyDialog");
		 			oDialog.open();
        	        var oJsonModel = new JSONModel("/flow7/api/dashboard/processing");
        	        that.getView().setModel(oJsonModel,"processingLists");
			        if ( that.getView("processingLists") ) {
			            resolve(oJsonModel);
			        } else {
			            reject("get json error");
			        }
			    });
        },
        
		_handleRouteMatched: function(oEvent) {
			//var diagramId = oEvent.getParameter("arguments")["?query"].diagramId;
            var diagramId = oEvent.getParameter("arguments").DiagramId;
			var list = this.getView().byId("lists");
			var binding = list.getBinding("items");
			//var binding = list.getBinding("rows");
			//console.log(binding);
			binding.filter(
				[new sap.ui.model.Filter([new sap.ui.model.Filter("DiagramId", sap.ui.model.FilterOperator.EQ, diagramId)]),
					false
				]);

			this.getView().byId("listPage").setTitle(oEvent.getParameter("arguments")["?query"].diagramName);
		},
		
		onListItemPressed: function (oEvent) {
			var oItem, oCtx,oBindItem;
			oItem = oEvent.getSource();
			//console.log(oItem);
			oCtx = oItem.getBindingContext("processingLists");
			//oCtx = oItem.getBindingContext("undefined");
			oBindItem = oCtx.getModel().getProperty(oCtx.getPath());
            if (oCtx.getProperty("DiagramId") === "FDP_P0")
            {
				//var oRouter =sap.ui.core.UIComponent.getRouterFor(this);
				this._oRouter.navTo("formDetail",{
					RequisitionId : oCtx.getProperty("RequisitionId"),
					query:{
						diagramName: oBindItem.DiagramName,
						diagramId: oBindItem.DiagramId
					}
				});	
            }
            else {
				MessageToast.show("... unavalible in POC project....", {
					
				});
			}
			
		}
	});
		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf Andy20171130.view.processingList
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf Andy20171130.view.processingList
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf Andy20171130.view.processingList
		 */
		//	onExit: function() {
		//
		//	}


});