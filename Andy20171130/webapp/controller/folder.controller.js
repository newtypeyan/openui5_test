sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History"
], function(Controller,JSONModel,History) {
	"use strict";

	return Controller.extend("Andy20171130.controller.folder", {

			onInit: function() {
				     //var oJsonModel = new JSONModel("/flow7/api/diagram");
        			//this.getView().setModel(oJsonModel,"ForderCategory");
        			
        			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
					this._oRouter.getRoute("folder").attachMatched(this._handleRouteMatched, this);
					this._oRouter.getRoute("fillForm").attachMatched(this._handleRouteMatched, this);
        			
			},
			
			_handleRouteMatched: function(oEvent) {
				//获取json数据
	            var FolderId = oEvent.getParameter("arguments").FolderGuid;
	            this._FolderGuid = FolderId;
                var oJson;				
			    $.ajax({
					type: "get",
					cache: false,
					url: "/flow7/api/diagram",
					async: false,
					dataType:"json",
					success: function(data){
						   //console.log(data);
							oJson = data; 
					},
					error: function(data,msg,aa)
					{
					   //console.log(msg.toString());
					}
				});
				//console.log(oJson);
				//过滤表单类别
				for(var i = 0; i < oJson.length; i++)
				{
					if (oJson[i].FolderGuid && oJson[i].FolderGuid === FolderId)
					{
						oJson = oJson[i];
						break;
					}
				}
				
				//绑定model	
			     var oJsonModel = new JSONModel(oJson);
    			this.getView().setModel(oJsonModel,"ForderCategory");
        	/*		
				var list = this.getView().byId("folderlist");
				var binding = list.getBinding("items");
				//console.log(FolderId);
				console.log(binding);
				binding.filter(
					[new sap.ui.model.Filter([new sap.ui.model.Filter("FolderGuid", sap.ui.model.FilterOperator.EQ, FolderId)]),
						false
					]);
	            //console.log(binding);
	        */
				//this.getView().byId("listPage").setTitle(oEvent.getParameter("arguments")["?query"].diagramName);
			},

			diagramFactory : function(sId,oContext) {
				var oUIControl = null;
				var sCategoryName = oContext.getProperty("DisplayName");
				if (oContext.getProperty("DiagramId") === "FDP_P0") {
					// Yup, so use a
					// StandardListItem
					oUIControl = new sap.m.StandardListItem(sId, {
						title : sCategoryName
					});
					oUIControl.setType(sap.m.ListType.Navigation);
					oUIControl.attachPress(this.onListItemPressed, this);
				} else {
					oUIControl = new sap.m.StandardListItem(sId, {
						title : sCategoryName
					});
					oUIControl.addStyleClass("myCustom");
				}
				return oUIControl;
			},
		
			onListItemPressed: function (oEvent) {
			var oItem, oCtx,oBindItem;
			oItem = oEvent.getSource();
			//console.log(oItem);
			oCtx = oItem.getBindingContext("ForderCategory");
			oBindItem = oCtx.getModel().getProperty(oCtx.getPath());
            //console.log(oBindItem); 
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("fillForm",{
					FolderGuid : oCtx.getProperty("FolderGuid"),
					//DiagramId : oBindItem.DiagramId, 
					query:{
						diagramName: oBindItem.DisplayName,
						diagramId: oBindItem.DiagramId
					}
					
				});	
            
		},
		
		onNavBack: function (oEvent) {
			var oHistory, sPreviousHash , oRouter;

			oHistory = History.getInstance();
			sPreviousHash = oHistory.getPreviousHash();
			oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            var FolderId = this._FolderGuid;
            
			jQuery.sap.require("sap.ui.core.EventBus");
			var oEventBus = sap.ui.getCore().getEventBus();
			oEventBus.publish("MasterChannel1","MasterBack",{
				FolderGuid : FolderId
			});
			
            
            if (FolderId)
            {
            	oRouter.navTo("master",{
            	/*	
				FolderGuid : FolderId,
					 query:{
						dt: new Date().getTime()	
					}
				*/	
            	});
            }
            /*
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				oRouter.navTo("master", {}, true ); // no history
			}
			*/
		}
		
		
		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf Andy20171130.view.folder
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf Andy20171130.view.folder
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf Andy20171130.view.folder
		 */
		//	onExit: function() {
		//
		//	}

	});

});