sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel"
], function(Controller,JSONModel) {
	"use strict";

	return Controller.extend("Andy20171130.controller.folderCategory", {

			onInit: function() {
				     var oJsonModel = new JSONModel("/flow7/api/diagram");
        			this.getView().setModel(oJsonModel,"ForderCategory");
        			
			},
			
			onListItemPressed: function (oEvent) {
			var oItem, oCtx,oBindItem;
			oItem = oEvent.getSource();
			//console.log(oItem);
			oCtx = oItem.getBindingContext("ForderCategory");
			oBindItem = oCtx.getModel().getProperty(oCtx.getPath());

				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("folder",{
					FolderGuid : oCtx.getProperty("FolderGuid")
					/*
					query:{
						diagramName: oBindItem.DiagramName,
						diagramId: oBindItem.DiagramId
					}
					*/
				});	
            
		},
		
		folderCategoryFactory : function(sId,oContext) {
			var oUIControl = null;
			var sCategoryName = oContext.getProperty("FolderName");
			if (oContext.getProperty("FolderGuid") === "FAMF") {
				// Yup, so use a
				// StandardListItem
				oUIControl = new sap.m.StandardListItem(sId, {
					title : sCategoryName
				});
				oUIControl.setType(sap.m.ListType.Navigation);
				oUIControl.attachPress(this.onListItemPressed, this);
			} else {
				oUIControl = new sap.m.StandardListItem(sId, {
					title : sCategoryName
				});
				oUIControl.addStyleClass("myCustom");
			}
			return oUIControl;
		}	

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf Andy20171130.view.folderCategory
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf Andy20171130.view.folderCategory
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf Andy20171130.view.folderCategory
		 */
		//	onExit: function() {
		//
		//	}

	});

});