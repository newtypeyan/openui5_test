sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"jquery.sap.global",
	"sap/ui/core/Fragment",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter",
	"sap/m/MessageToast",
	"jquery.sap.storage"
], function(Controller,JQuery,Fragment,JSONModel,Filter,MessageToast,storage) {
	"use strict";

	return Controller.extend("Andy20171130.controller.Form_FDP_P0", {
		
		onInit: function() {

			jQuery.sap.require("sap.ui.core.EventBus");
			var oEventBus = sap.ui.getCore().getEventBus();
			oEventBus.publish("DetailChannel1","FillForm",{
			});
			
			     jQuery.sap.require("jquery.sap.storage");
			     var oJQueryStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			     var oJsonModel;
			     //console.log(oJQueryStorage.get("myFormStorage"));
			     //oJQueryStorage.removeAll();
			    if (oJQueryStorage.get("myFormStorage"))
			    {
			    	var oModel = JSON.parse(oJQueryStorage.get("myFormStorage"));
			    	oJsonModel = new JSONModel(oModel);
			    }
			    else
			    {
				     oJsonModel = new JSONModel("/flow7/api/fdp");
			    }
			    this.getView().setModel(oJsonModel,"Diagram");
			    this.getView().getModel("Diagram").attachPropertyChange(this.FormModelChange,this);
 			     var oJsonModel2 = new JSONModel("/erp/api/purma");
    			this.getView().setModel(oJsonModel2,"Purma");  
    			
 			     var oJsonModel3 = new JSONModel("/erp/api/cmsme");
    			this.getView().setModel(oJsonModel3,"Cmsme");   
    			
 			     var oJsonModel4 = new JSONModel("/erp/api/cmsnb");
    			this.getView().setModel(oJsonModel4,"Cmsnb"); 
    			
  			     var oJsonModel5 = new JSONModel("/erp/api/receipt");
    			this.getView().setModel(oJsonModel5,"Receipt");    			
    			
			    //this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				//this._oRouter.getRoute("fillForm").attachMatched(this._handleRouteMatched, this);
				
				
    			
		},
		
		//使用EventBus当表单Model改变时发布Model数据给fillFormControl
		FormModelChange : function (oEvent) {
			var oModel = this.getView().getModel("Diagram");
			jQuery.sap.require("sap.ui.core.EventBus");
			var oEventBus = sap.ui.getCore().getEventBus();
			oEventBus.publish("channel1","modelChanged",{
				FormModel : oModel
			});
			
		},
		
		//Select Supplier 
		inputId : "" ,
		
		handleSupplierValueHelp : function (oEvent) {
			
			var sInputValue = oEvent.getSource().getValue();
			this.inputId = oEvent.getSource().getId();
			
			// create value help dialog
			if (!this._valueSupplierHelpDialog) {
				this._valueSupplierHelpDialog = sap.ui.xmlfragment(
					"Andy20171130.view.PaySupplier",
					this
				);
				this.getView().addDependent(this._valueSupplierHelpDialog);
			}

			// create a filter for the binding
			/*
			if (sInputValue !== "" && sInputValue !== undefined)
			{
				this._valueSupplierHelpDialog.getBinding("items").filter([new Filter(
					"Ma003",
					sap.ui.model.FilterOperator.Contains, sInputValue
				)]);
			}
			*/
			// open value help dialog filtered by the input value
			this._valueSupplierHelpDialog.open();  //para  sInputValue
		},

		_handleSupplierValueHelpSearch : function (evt) {
			var sValue = evt.getParameter("value");
			var oFilter = new Filter(
				"Ma003",
				sap.ui.model.FilterOperator.Contains, sValue
			);
			evt.getSource().getBinding("items").filter([oFilter]);
		},

		_handleSupplierValueHelpClose : function (evt) {
			//var oSelectedItem = evt.getParameter("selectedItem");
			var aContexts = evt.getParameter("selectedContexts");
			if (aContexts)
			{			
	/*			
	            for(var i = 0; i < aContexts.length; i++)
	            {
				var oCtx = aContexts[i]; //
				var oBindItem = oCtx.getModel().getProperty(oCtx.getPath());
				console.log(oBindItem);
	            }
	*/            
	            var sSupplierName = aContexts.map(function(oContext) { 
	            	var oBindItem = oContext.getModel().getProperty(oContext.getPath());
	            	return oBindItem.Ma003; 
	            }).join(", ");
	            var sSupplierId = aContexts.map(function(oContext) { 
	            	var oBindItem = oContext.getModel().getProperty(oContext.getPath());
	            	return oBindItem.Ma005; 
	            }).join(", "); 
	            var sCurrency = aContexts.map(function(oContext) { 
	            	var oBindItem = oContext.getModel().getProperty(oContext.getPath());
	            	return oBindItem.Ma021; 
	            }).join(", ");             
	            //console.log(sSupplierName);
				if (sSupplierName) {
					//var SupplierInput = this.getView().byId(this.inputId),
					var	oText1 = this.getView().byId("PaySupplierId");
					var oText2 = this.getView().byId("PaySupplierName");
					var oText3 = this.getView().byId("Currency");

					oText1.setValue(sSupplierId);
					oText2.setValue(sSupplierName);
					oText3.setValue(sCurrency);
				}
			}
			//console.log(this._valueSupplierHelpDialog.getBindingInfo("items").template);
			evt.getSource().getBinding("items").filter([]);
		},

		suggestionSupplierItemSelected: function (evt) {
			var oItem = evt.getParameter("selectedItem"),
				oText = this.getView().byId("PaySupplierId"),
				sKey = oItem ? oItem.getKey() : "";
            //console.log(oItem);
			oText.setText(sKey);
		},		


        // Dept Select 
		handleDeptValueHelp : function (oEvent) {
			// create value help dialog
			if (!this._valueDeptHelpDialog) {
				this._valueDeptHelpDialog = sap.ui.xmlfragment(
					"Andy20171130.view.HomeDept",
					this
				);
				this.getView().addDependent(this._valueDeptHelpDialog);
			}

			// open value help dialog filtered by the input value
			this._valueDeptHelpDialog.open();
		},

		_handleDeptValueHelpSearch : function (evt) {
			var sValue = evt.getParameter("value");
			var oFilter = new Filter(
				"Me002",
				sap.ui.model.FilterOperator.Contains, sValue
			);
			evt.getSource().getBinding("items").filter([oFilter]);
		},

		_handleDeptValueHelpClose : function (evt) {
			var aContexts = evt.getParameter("selectedContexts");
			if (aContexts)
			{
	            var sDeptName = aContexts.map(function(oContext) { 
	            	var oBindItem = oContext.getModel().getProperty(oContext.getPath());
	            	return oBindItem.Me002; 
	            }).join(", ");
	            var sDeptId = aContexts.map(function(oContext) { 
	            	var oBindItem = oContext.getModel().getProperty(oContext.getPath());
	            	return oBindItem.Me001; 
	            }).join(", "); 
	            
	            //console.log(sSupplierName);
				if (sDeptName) {
					var	oText1 = this.getView().byId("HomeDept");
					//var oText2 = this.getView().byId("HomeDeptId");
	
					oText1.setValue(sDeptName);
					//oText2.setValue(sDeptId);
				}
			}
			evt.getSource().getBinding("items").filter([]);
			
		},
		
        // select Project 
		handleProjectValueHelp : function (oEvent) {
			// create value help dialog
			if (!this._valueProjectHelpDialog) {
				this._valueProjectHelpDialog = sap.ui.xmlfragment(
					"Andy20171130.view.ProjectCode",
					this
				);
				this.getView().addDependent(this._valueProjectHelpDialog);
			}

			// open value help dialog filtered by the input value
			this._valueProjectHelpDialog.open();
		},

		_handleProjectValueHelpSearch : function (evt) {
			var sValue = evt.getParameter("value");
			var oFilter = new Filter(
				"Nb002",
				sap.ui.model.FilterOperator.Contains, sValue
			);
			evt.getSource().getBinding("items").filter([oFilter]);
		},

		_handleProjectValueHelpClose : function (evt) {
			var aContexts = evt.getParameter("selectedContexts");
			if (aContexts)
			{
	            var sProjectName = aContexts.map(function(oContext) { 
	            	var oBindItem = oContext.getModel().getProperty(oContext.getPath());
	            	return oBindItem.Nb002; 
	            }).join(", ");
	            var sProjectId = aContexts.map(function(oContext) { 
	            	var oBindItem = oContext.getModel().getProperty(oContext.getPath());
	            	return oBindItem.Nb001; 
	            }).join(", "); 
	            
	            //console.log(sSupplierName);
				if (sProjectName) {
					var	oText1 = this.getView().byId("ProjectCode");
					//var oText2 = this.getView().byId("ProjectId");
	
					oText1.setValue(sProjectId);
					//oText2.setValue(sProjectName);
				}
			}
			evt.getSource().getBinding("items").filter([]);
			
		},
		
		//支付类别下拉联动发票类别和税收类别
		handleReceiptChange : function (evt) {
			//var	oSelectReceipt = this.getView().byId("DocumentCategory");
			var oInvoiceType = this.getView().byId("InvoiceType");
			var oTaxationType =  this.getView().byId("TaxationType");
			//var oModel = this.getView().getModel("Receipt");
			var oContext = evt.getParameters().selectedItem.getBindingContext("Receipt");
			var oBindData = oContext.getProperty(oContext.getPath());
			//console.log(oContext.getProperty(oContext.getPath()));
			var aInvoiceType = oBindData.Invoice;
			var aTaxationType = oBindData.Tax;
			if (aInvoiceType)
			{
				oInvoiceType.removeAllItems();
				for(var i = 0; i < aInvoiceType.length; i++)
				{
					oInvoiceType.addItem(new sap.ui.core.Item({text:aInvoiceType[i]}));
					if (i === 0)
					{
						oInvoiceType.setSelectedItem(aInvoiceType[i]);
					}
				}
			}
			if (aTaxationType)
			{
				oTaxationType.removeAllItems();
				for(var j = 0; j < aTaxationType.length; j++)
				{
					oTaxationType.addItem(new sap.ui.core.Item({text:aTaxationType[j]}));
					if (j === 0)
					{
						oTaxationType.setSelectedItem(aTaxationType[j]);	
					}
				}
			}
		},
		
		//计算合计金额	
		handleTotalPress : function (evt) {
			var sNoTaxAmount = this.getView().byId("NoTaxAmount").getValue();
			var sTax = this.getView().byId("Tax").getValue();
			var oTotal = this.getView().byId("Total");
			if (sNoTaxAmount !== "" &&  !isNaN(parseFloat(sNoTaxAmount.replace(/,/g,""))))
			{
				if (sTax !== "" &&  !isNaN(parseFloat(sTax.replace(/,/g,""))))
				{
					oTotal.setValue(parseFloat(sNoTaxAmount.replace(/,/g,"")) + parseFloat(sTax.replace(/,/g,"")));
				}
				else
				{
					oTotal.setValue(parseFloat(sNoTaxAmount.replace(/,/g,"")));
				}
			}
			
		}
		
		
		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf Andy20171130.view.Form_FDP_P0
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf Andy20171130.view.Form_FDP_P0
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf Andy20171130.view.Form_FDP_P0
		 */
		//	onExit: function() {
		//
		//	}

	});

});