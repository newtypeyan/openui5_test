sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("Andy20171130.controller.App", {
        onInit: function() {
        	jQuery.sap.require("sap.ui.core.EventBus");
			var oEventBus = sap.ui.getCore().getEventBus();
			oEventBus.subscribe("MasterChannel1", "MasterBack", this.MasterBack, this);
			//oEventBus.subscribe("DetailChannel1", "FillForm", this.FillForm, this);
        },
        
        MasterBack : function() {
        	//console.log("back");
        	this.getSplitAppObj().removeAllDetailPages();
        },
        
        FillForm : function() {
        	this.getSplitAppObj().removeDetailPage("listPage");
        },
        
		getSplitAppObj : function() {
			var result = this.getView().byId("app");
			if (!result) {
				jQuery.sap.log.info("app object can't be found");
			}
			return result;
		}        
	});
});