sap.ui.define([
	"sap/ui/model/json/JSONModel"
], function(JSONModel) {
	"use strict";

	return {
		processingStepText: function(sProcessing) {
		   var str = "";
		   var arrProc = sProcessing.split("]][[");
		   var arrStep ;
		   if (arrProc[0] !== undefined && arrProc[0] !== "" )
		   {
		   	  arrStep = arrProc[0].split("]@[");
		      if (arrStep.length > 0)
		      {
		      	str = arrStep[1];
		      }
		   }
		   return str;
		},
		processingStepName: function(sProcessing) {
		   var str = "";
		   var arrProc = sProcessing.split("]][[");
		   var arrStep ;
		   if (arrProc[0] !== undefined && arrProc[0] !== "")
		   {
		   	  arrStep = arrProc[0].split("]@[");
		      if (arrStep.length > 0)
		      {
		      	str = arrStep[2];
		      }
		   }
		   return str;
		},
		
		processingStatus: function(intStatus){
			var resourceBundle = this.getView().getModel("i18n").getResourceBundle();
			var strResult = resourceBundle.getText("processingStatusA");
			switch (intStatus) {
				case 0:
					return resourceBundle.getText("processingStatusA");
				case 1:
					return resourceBundle.getText("processingStatusB");
				case 2:
					return resourceBundle.getText("processingStatusC");
				default:
					return strResult;
			}			
		}
		
	};
});